Jakub Kazimír

Prehľad

Aplikácia slúži na porovnanie presnosti predpovede počasia. Zahŕňa tieto scenáre:

Užívateľ si po príchode na stránky môže pozrieť predpoveď počasia na základe najlepšej dosiahnutej úspešnosti, aktuálne počasie a predpokladanú presnosť jednotlivých meraní. Stránka by mala umožniť zozbierať dáta z inej stránky, čo znamená pristúpiť k stránke, vybrať mesto, stiahnuť z nej dáta a priradiť ich do správnej tabuľky v databáze. Zozbierané dáta predpovede počasia by mal systém prečítať, vypočítať a aktualizovať mieru úspešnosti predpovede pre kombináciu daného miesta, stránky a dňa predpovede a uložiť ju do databázy k príslušnému miestu. Stránka by mala umožniť dáta pre jednotlivé dni spriemerovať a zobraziť všeobecné štatistiky. Užívateľ by mal možnosť prezrieť si históriu aktuálneho počasia, ktoré si môže vyhľadať na základe rôznych parametrov. Užívateľ by mal mať možnosť prezrieť si jednotlivé štatistiky aj s údajmi, ktoré boli použité na ich výpočet. Backend aplikácie je vytvorený v programovacom jazyku Ruby s frameworkom Rails. Frontend je vytvorený v html. V projekte používam databázu Postgresql.

Frontend

Hlavná stránka (weather.html) obsahuje informácie o aktuálnej teplote, presnosti aktuálne zobrazenej predpovede a hodinovú predpoveď počasia. Všetky údaje sú prevzaté z databázy. Táto stránka má za úlohu iba dané údaje zobrazovať, umožniť užívateľovi vybrať na základe akých parametrov (mesto a dátum predpovede) chce zobraziť údaje a tie odoslať do controlera. Vedľajšie stránky (stats.html,history.html) slúžia na rozšírenie hlavnej stránky. Stats.html zobrazuje jednotlivé štatistiky pre mestá a stránky, spolu s údajmi, z ktorých tieto štatistiky vznikali. Taktiež si užívateľ môže vybrať pre akú stránku (zatiaľ bola implementovaná 1 stránka) a aké mesto chce dané štatistiky zobraziť. History.html slúži na zobrazenie jednotlivých aktuálnych počasí pre rôzne mestá spolu s vyhľadávaním záznamov. Obidve stránky slúžia iba na prezentáciu záznamov v databáze a posielaní parametrov, ktoré zadá užívateľ do controlera. Na jednotlivé komponenty je použitý css štýl bootstrap.

Backend

Backend je napísaný v jazyku ruby on rails, ktorý používam na získavanie dát z iných stránok, generovanie obsahu html stránok a prácu s databázou. Taktiež ho používam na prepočet štatistík a vyhľadávanie v databáze. Na stiahnutie počasia používam gemy HTTParty a Patron a funkcie, ktoré sú uložené v modely. Výpočet štatistík prebieha v 2 krokoch. 1. krok je pri stiahnutí aktuálneho počasia, kedy program vyberie všetky záznamy počasia pre daný deň a hodinu a vypočíta pre každý záznam jeho úspešnosť. Túto úspešnosť pre jednotlivé parametre uloží do daného záznamu. 2. krok je vybratie všetkých spoločných záznamov podľa stránky, mesta a dňa predpovede a vytvorenie jednotnej štatistiky pre nich. Dáta pre predpoveď počasia získavam pomocou gemu Patron, ktoré spracovávam pomocou Nokogiri a ukladám do databázy. Aktuálne počasie získavam pomocou gemu HTTParty, keďže stránka bola proti sťahovaniu dát cez Patron zabezpečená. Dáta následne spracovávam pomocou Nokogiri a ukladám do databázy pomocou sql príkazov. Posielanie dát na frontend funguje pomocou atribútov, ktoré prichádzajú naspäť z frontendu. Dáta vyhľadám v tabuľke na základe nových atribútov a tieto dáta sa posielajú na frontend, kde sa nanovo vykreslia. Pri práci s databázou používam príkazy find_by_sql a ActiveRecord::Base.connection, ktorý zatváram hneď po vykonaní sérií inštrukcií.

Ukážky vyhľadávania: Vyhľadanie počasia pre dané mesto a daný deň (deň 0 znamená aktuálny deň) GET /?city=1&day=0

Vyhľadanie záznamu v histórií aktuálnych počasí GET /history?utf8=✓&city=Bratislava&date=2016-04-10&hour=2&temp=6&wind=7&hum=7&commit=Vyhľadať

Odpoveďou na tieto volania sú polia hash tabuliek, v ktorých sú uložené dáta, ktoré boli na základe atribútov požadované. Pre prvé vyhľadávanie, ktoré sa nachádza na hlavnej stránke, vráti aktuálne počasie pre mesto - city, štatistiku pre dané mesto a daný deň - day, a predpoveď počasia.

Databáza

Dáta z predpovedí získavam zo stránky sme.sk, pričom je možné ďalšie rozšírenie o predpovede z ostatných stránok. Dáta aktuálnej predpovede získavam zo stránky meteo.sk. Údaje, ktoré z týchto stránok sťahujem sú: teplota vzduchu, rýchlosť vetra, vlhkosť vzduchu, slovný opis počasia. Predpovede počasia (weather_predictions) obsahujú kolónky pre uloženie dňa a hodiny, pre ktoré platí daná predpoveď, teploty, rýchlosti vetra, vlhkosti, a kolonky pre výpočet presnosti jednotlivých údajov. Aktuálne počasia (weathers) obsahuje kolonky pre deň a hodinu, kedy bola aktuálna predpoveď zozbieraná, teplotu, rýchlosť vetra a vlhkosť vzduchu. Mestá (cities) obsahujú kolonky pre mená miest. Stránky (pages) sú stránky, z ktorých sťahujem predpovede počasia a obsahujú kolónku pre názov stránky a adresu. Presnosti (accurancies) je tabuľka, ktorá obsahuje presnosti predpovede pre kombinácie stránok, miest a dní. Obsahuje kolonky pre presnosti teploty, rýchlosti vetra a vlhkosti.

Poznámka

Program je závislí od aktuálnych dát pre každý deň a každú hodinu. Keďže táto aplikácia nie je nasadená, aktuálne dáta v aplikácií budú chýbať, čo môže spôsobiť jej nestabilitu alebo nepredvídané správanie. Aplikácia sa spustí bez aktualizácie a dáta v nej budú staršieho dáta.