require 'test_helper'

class AboutControllerTest < ActionController::TestCase
  test "should get About" do
    get :About
    assert_response :success
    assert_select "title", "WPC - O Projekte"
  end

  test "should get contact" do
    get :Contact
    assert_response :success
    assert_select "title", "WPC - Kontakt"
  end
end
