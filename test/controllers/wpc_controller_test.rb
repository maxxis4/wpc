require 'test_helper'

class WpcControllerTest < ActionController::TestCase
  test "should get weather" do
    get :weather
    assert_response :success
  end

  test "should get stats" do
    get :stats
    assert_response :success
  end

end
