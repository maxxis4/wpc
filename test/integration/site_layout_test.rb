require 'test_helper'

class SiteLayoutTest < ActionDispatch::IntegrationTest
  test "layout links" do
    get root_path
    assert_template 'wpc/weather'
    assert_select "a[href=?]", root_path, count: 2
    assert_select "a[href=?]", stats_path
    assert_select "a[href=?]", about_path
    #assert_select "a[href=?]", contact_path
  end

  test "actual weather show on main page" do
    get root_path
    assert_template 'wpc/weather'
    Cities.all.each do |city|
      assert_select "a[href=/#{@city_hash[city]}]"
    end
  end
end
