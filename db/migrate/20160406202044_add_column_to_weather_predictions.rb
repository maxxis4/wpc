class AddColumnToWeatherPredictions < ActiveRecord::Migration
  def change
    add_column :weather_predictions, :day, :integer
    add_column :weather_predictions, :temperature_a, :integer
    add_column :weather_predictions, :wind_speed_a, :integer
    add_column :weather_predictions, :humidity_a, :integer
  end
end
