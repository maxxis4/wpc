class CreateAccurancies < ActiveRecord::Migration
  def change
    create_table :accurancies do |t|
      t.belongs_to :city
      t.belongs_to :page
      t.integer :temperature_a
      t.integer :wind_speed_a
      t.integer :humidity_a

      t.timestamps null: false
    end

  end
end
