class PredictionMaterializedView < ActiveRecord::Migration
  def up
    connection.execute <<-SQL
      CREATE MATERIALIZED VIEW weather_predictions_actual AS
        SELECT * FROM (
          SELECT 	*,
            dense_rank() over(PARTITION BY city_name,day ORDER BY hodnoty DESC) AS rank
          FROM (
            select c.city_name,
              wp.city_id,
              wp.date,wp.hour,
              wp.temperature,
              wp.wind_speed,
              wp.humidity,
              wp.description,
              wp.day,
              p.page_name,
              p.address,
              a.temperature_a,
              a.wind_speed_a,
              a.humidity_a,
              (a.temperature_a*1.7 + a.wind_speed_a*1.4 + a.humidity_a*1.2) as hodnoty
            from weather_predictions wp
            join cities c on wp.city_id=c.id
            join pages p on p.id = wp.page_id
            join accurancies a on a.page_id = p.id
            where wp.date-wp.day = current_date +1
            and wp.day = a.day
            and wp.city_id = a.city_id
            order by p.id,wp.day,wp.hour
            ) as weat_pred
          ) as weather
        where rank = 1;
      CREATE INDEX ON weather_predictions_actual (city_name);
    SQL
  end

  def down
    connection.execute <<-SQL
      DROP MATERIALIZED VIEW IF EXISTS weather_predictions_actual;
    SQL
  end
end