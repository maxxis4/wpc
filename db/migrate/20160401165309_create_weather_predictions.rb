class CreateWeatherPredictions < ActiveRecord::Migration
  def change
    create_table :weather_predictions do |t|
      t.belongs_to :city
      t.belongs_to :page
      t.belongs_to :weather
      t.date :Date
      t.integer :Hour
      t.integer :Temperature
      t.integer :Wind_speed
      t.integer :Humidity
      t.string :Weather

      t.timestamps null: false
    end
  end
end
