class ToLower < ActiveRecord::Migration
  def change
    rename_column :pages, :Name, :name
    rename_column :cities, :Name, :name
    rename_column :weathers, :Date, :date
    rename_column :weathers, :Hour, :hour
    rename_column :weathers, :Temperature, :temperature
    rename_column :weathers, :Wind_speed, :wind_speed
    rename_column :weathers, :Humidity, :humidity
    rename_column :weathers, :Weather, :weather
    rename_column :weather_predictions, :Date, :date
    rename_column :weather_predictions, :Hour, :hour
    rename_column :weather_predictions, :Temperature, :temperature
    rename_column :weather_predictions, :Wind_speed, :wind_speed
    rename_column :weather_predictions, :Humidity, :humidity
    rename_column :weather_predictions, :Weather, :weather
  end
end
