class CreateWeathers < ActiveRecord::Migration
  def change
    create_table :weathers do |t|
      t.belongs_to :city
      t.date :Date
      t.integer :Hour
      t.integer :Temperature
      t.integer :Wind_speed
      t.integer :Humidity
      t.string :Weather

      t.timestamps null: false
    end
  end
end
