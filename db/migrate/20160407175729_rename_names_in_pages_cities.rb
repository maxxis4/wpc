class RenameNamesInPagesCities < ActiveRecord::Migration
  def change
    rename_column :pages, :name, :page_name
    rename_column :cities, :name, :city_name
  end
end
