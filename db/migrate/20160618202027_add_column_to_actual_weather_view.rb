class AddColumnToActualWeatherView < ActiveRecord::Migration
  def up
    connection.execute <<-SQL
      DROP MATERIALIZED VIEW IF EXISTS actual_weather_actual;
      CREATE MATERIALIZED VIEW actual_weather_actual AS
        select c.city_name, c.id, c.country_id, co.name, w.temperature, w.wind_speed, w.humidity, w.description, w.pressure
        from weathers w
        join cities c on c.id=w.city_id
        join countries co on co.id = c.country_id
        where w.created_at::date = current_date::date
        and extract('hour' from w.created_at) = extract('hour' from timezone('UTC', now()));
    SQL
  end

  def down
    connection.execute <<-SQL
      DROP MATERIALIZED VIEW IF EXISTS actual_weather_actual;
    SQL
  end
end
