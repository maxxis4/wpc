class CreatePages < ActiveRecord::Migration
  def change
    create_table :pages do |t|
      t.string :address
      t.string :Name

      t.timestamps null: false
    end
  end
end
