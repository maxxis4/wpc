class AddColumnsForPressure < ActiveRecord::Migration
  def change
    add_column :accurancies, :pressure_a, :integer
    add_column :weathers, :pressure, :integer
    add_column :weather_predictions, :pressure, :integer
    add_column :weather_predictions, :pressure_a, :integer
  end
end
