# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
#
#       City.create("city_name" => "Bratislava", "country_id" => 1 )
#       City.create("city_name" => "Trnava", "country_id" => 2)
#       City.create("city_name" => "Nitra", "country_id" => 3)
# # #     City.create("Name" => "Trenčín", "country_id" => 4)
#      City.create("city_name" => "Žilina", "country_id" => 5)
#       City.create("city_name" => "Banská Bystrica", "country_id" => 6)
#       City.create("city_name" => "Prešov" , "country_id" => 7)
#       City.create("city_name" => "Košice", "country_id" => 8)
#
# Page.create("page_name" => "Sme.sk", "address" => "http://pocasie.sme.sk/krajina/slovensko/")
# Page.create("page_name" => "Počasie.sk", "address" => "http://www.pocasie.sk/slovensko/")
# Page.create("page_name" => "Meteobox.sk", "address" => "https://meteobox.sk/")
# Page.create("page_name" => "Freemeteo.sk", "address" => "http://freemeteo.sk/")
#  Page.create("page_name" => "Svieti.com", "address" => "http://www.svieti.com/")
#
#  (1..7).each do |i|
#    (0..5).each do |day|
#      Accurancy.create("city_id" => i, "page_id" => 5, "temperature_a" => 0, "wind_speed_a" => 0, "humidity_a" => 0, "day" => day)
#    endf
#  end
#  City.create("city_name" => "Bratislava", "country_id" => 1 )
#  City.create("city_name" => "Trnava", "country_id" => 2)
#  City.create("city_name" => "Nitra", "country_id" => 3)
# # City.create("Name" => "Trenčín", "country_id" => 4)
#  City.create("city_name" => "Žilina", "country_id" => 5)
#  City.create("city_name" => "Banská Bystrica", "country_id" => 6)
#  City.create("city_name" => "Prešov" , "country_id" => 7)
#  City.create("city_name" => "Košice", "country_id" => 8)
#  City.create("city_name" => "Detva", "country_id" => 6)
#  City.create("city_name" => "Gelnica", "country_id" => 8)
#  City.create("city_name" => "Humenné", "country_id" => 7)
#  City.create("city_name" => "Kamenica nad Cirochou", "country_id" => 7)
#  City.create("city_name" => "Kežmarok", "country_id" => 7)
#  City.create("city_name" => "Lučenec", "country_id" => 6)
#  City.create("city_name" => "Malacky", "country_id" => 1)
#  City.create("city_name" => "Martin", "country_id" => 5)
#  City.create("city_name" => "Michalovce", "country_id" => 8)
#  City.create("city_name" => "Partizánske", "country_id" => 4)
#  City.create("city_name" => "Piešťany", "country_id" => 2)
#  City.create("city_name" => "Poltár", "country_id" => 6)
#  City.create("city_name" => "Poprad", "country_id" => 7)
#  City.create("city_name" => "Prievidza", "country_id" => 4)
#  City.create("city_name" => "Revúca", "country_id" => 6)
#  City.create("city_name" => "Sliač", "country_id" => 6)
#  City.create("city_name" => "Stropkov", "country_id" => 7)
#  City.create("city_name" => "Trebišov", "country_id" => 8)
#  City.create("city_name" => "Vranov nad Topľou", "country_id" => 7)
#  City.create("city_name" => "Žarnovica", "country_id" => 6)
#
#  Country.create("name" => "Bratislavský kraj", "main_city_id" => 1)
#  Country.create("name" => "Trnavský kraj", "main_city_id" => 2)
#  Country.create("name" => "Nitriansky kraj", "main_city_id" => 3)
#  Country.create("name" => "Trenčiansky kraj", "main_city_id" => 17)
#  Country.create("name" => "Žilinský kraj", "main_city_id" => 4)
#  Country.create("name" => "Banskobystrický kraj", "main_city_id" => 5)
#  Country.create("name" => "Prešovský kraj", "main_city_id" => 6)
#  Country.create("name" => "Košický kraj", "main_city_id" => 7)

# (1..27).each do |i|
#   (1..5).each do |j|
#    (0..5).each do |day|
#       Accurancy.create("city_id" => i, "page_id" => j, "temperature_a" => 0, "wind_speed_a" => 0, "humidity_a" => 0, "pressure_a" => 0, "day" => day)
#     end
#   end
#  end