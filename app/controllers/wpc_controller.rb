class WpcController < ApplicationController
  def weather
    puts params[:name]
    country_id = Country.get_id(params[:name])
    @actual_weather = Weather.find_by_sql <<-SQL
      SELECT * FROM actual_weather_actual where country_id = #{country_id}
    SQL
  end


  def stats
    @city = params[:name]
    @page = params[:page]
    @page_hash =  {'Počasie.sk' => 'pocasie' , 'Sme.sk' => 'sme', 'Meteobox.sk' => 'meteobox', 'Freemeteo.sk' => 'freemeteo', 'Svieti.com' => 'svieti'}

    if params[:name].nil? & @city.nil?
      city_id = 1
      @city = 'Bratislava'
    else
      city_id = City.get_id(@city)
    end
    if params[:page].nil?
      page_id = 1
      @page = 'sme'
    else
      @page = params[:page]
      page_id = Page.get_id(params[:page])
    end
    pages = Page.all
    act_temp = Array.new
    act_wind = Array.new
    act_hum = Array.new
    act_hour = Array.new
    act_pres = Array.new

    pred_temp = Array.new
    pred_wind = Array.new
    pred_hum = Array.new
    pred_pres = Array.new
    if params[:day].nil?
      day=0
    else
      day = params[:day].to_i
    end
    pages.each_with_index do |page,i|
      pred_temp[i+1] = Array.new
      pred_wind[i+1] = Array.new
      pred_hum[i+1] = Array.new
      pred_pres[i+1] = Array.new
    end

    w = Weather.find_by_sql(['
      select * from (
        select
          COALESCE(wp.page_id,0) as page_id,
          w.date,
          w.hour,
          w.temperature as actual_temperature,
          w.wind_speed as actual_wind_speed,
          w.humidity as actual_humidity,
          w.pressure as actual_pressure,
          wp.temperature,
          wp.wind_speed,
          wp.humidity,
          wp.pressure,
	        wp.day,
          dense_rank() over (order by w.date)-1 as day_order
        from weathers w
        left join weather_predictions wp on wp.weather_id = w.id
        where w.city_id = ?
        and w.date > current_date-6
        and w.date < current_date-2
        ) as a
      order by date,hour,page_id',
                             city_id])

    (0..2).each do
      (0..23).each do |i|
        act_hour.push(i)
        act_temp.push(nil)
        act_hum.push(nil)
        act_wind.push(nil)
        act_pres.push(nil)
        pages.each_with_index do |page,j|
          pred_temp[j+1].push(nil)
          pred_wind[j+1].push(nil)
          pred_hum[j+1].push(nil)
          pred_pres[j+1].push(nil)
        end
      end
    end
    w.each do |we|
      if (we.page_id == 0) || (we.page_id == 1  && we.hour != act_hour.last)
        act_temp[we.day_order*24+we.hour] = we.actual_temperature
        if we.actual_humidity.nil? || we.actual_humidity < 0
        else
          act_hum[we.day_order*24+we.hour] = we.actual_humidity
        end
        act_wind[we.day_order*24+we.hour] = we.actual_wind_speed
        act_pres[we.day_order*24+we.hour] = we.actual_pressure
      end
      if we.day == day && page_id != 0
        pred_temp[we.page_id][we.day_order*24+we.hour] = we.temperature
        pred_hum[we.page_id][we.day_order*24+we.hour] = we.humidity
        pred_wind[we.page_id][we.day_order*24+we.hour] = we.wind_speed
        if !we.pressure.nil? && we.pressure > 0
          pred_pres[we.page_id][we.day_order*24+we.hour] = we.pressure
        end
      end
    end



    @chart_temp = LazyHighCharts::HighChart.new('graph') do |f|
      f.title({ :text=>"Porovnanie nameraných a predpovedaných teplôt"})
      f.legend( layout: 'horizontal')
      f.options[:xAxis][:categories] = act_hour
      f.yAxis [
                  {title: {text: "Teplota v °C", margin: 20} },
                  {title: {text: "Teplota v °C"}, opposite: true},
              ]
      f.series(:type=> 'spline',:name=> 'Nameraná teplota',:data=> act_temp, :connectNulls => true)
      pages.each_with_index do |page,i|
        if page.id == page_id
          f.series(:type=> 'spline',:name=> "Teplota podľa #{page.page_name}",:data=> pred_temp[i+1], :connectNulls => true)
        else
          f.series(:type=> 'spline',:name=> "Teplota podľa #{page.page_name}",:data=> pred_temp[i+1], :connectNulls => true, :visible => false)
        end

      end
    end

    @chart_wind = LazyHighCharts::HighChart.new('graph') do |f|
      f.title({ :text=>"Porovnanie nameraných a predpovedaných rýchlostí vetra"})
      f.legend( layout: 'horizontal')
      f.options[:xAxis][:categories] = act_hour
      f.yAxis [
                  {title: {text: "Rýchlosť vetra v km/h", margin: 20} },
                  {title: {text: "Rýchlosť vetra v km/h"}, opposite: true},
              ]
      f.series(:type=> 'spline',:name=> 'Nameraná rýchlosť vetra',:data=> act_wind, :connectNulls => true)
      pages.each_with_index do |page,i|
        if page.id == page_id
          f.series(:type=> 'spline',:name=> "Rýchlosť vetra podľa #{page.page_name}",:data=> pred_wind[i+1], :connectNulls => true)
        else
          f.series(:type=> 'spline',:name=> "Rýchlosť vetra podľa #{page.page_name}",:data=> pred_wind[i+1], :connectNulls => true, :visible => false)
        end

      end
    end
    @chart_hum = LazyHighCharts::HighChart.new('graph') do |f|
      f.title({ :text=>"Porovnanie nameraných a predpovedaných vlhkostí vzduchu"})
      f.legend( layout: 'horizontal')
      f.options[:xAxis][:categories] = act_hour
      f.yAxis [
                  {title: {text: "Vlhkosť vzduchu v %", margin: 20} },
                  {title: {text: "Vlhkosť vzduchu v %"}, opposite: true},
              ]
      f.series(:type=> 'spline',:name=> 'Nameraná vlhkosť',:data=> act_hum, :connectNulls => true)
      pages.each_with_index do |page,i|
        if page.id == page_id
          f.series(:type=> 'spline',:name=> "Vlhkosť podľa #{page.page_name}",:data=> pred_hum[i+1], :connectNulls => true)
        else
          f.series(:type=> 'spline',:name=> "Vlhkosť podľa #{page.page_name}",:data=> pred_hum[i+1], :connectNulls => true, :visible => false)
        end

      end


    end

    @chart_pres = LazyHighCharts::HighChart.new('graph') do |f|
      f.title({ :text=>"Porovnanie nameraného a predpovedaného atmosférického tlaku"})
      f.legend( layout: 'horizontal')
      f.options[:xAxis][:categories] = act_hour
      f.yAxis [
                  {title: {text: "Atmosférický tlak v hPa", margin: 20} },
                  {title: {text: "Atmosférický tlak v hPa"}, opposite: true},
              ]
      f.series(:type=> 'spline',:name=> 'Nameraný tlak',:data=> act_pres, :connectNulls => true)
      pages.each_with_index do |page,i|
        if page.id == page_id
          f.series(:type=> 'spline',:name=> "Vlhkosť podľa #{page.page_name}",:data=> pred_pres[i+1], :connectNulls => true)
        else
          f.series(:type=> 'spline',:name=> "Vlhkosť podľa #{page.page_name}",:data=> pred_pres[i+1], :connectNulls => true, :visible => false)
        end

      end


    end

    @chart_globals = LazyHighCharts::HighChartGlobals.new do |f|
      f.global(useUTC: false)
      f.chart(
          backgroundColor: {
              linearGradient: [0, 0, 500, 500],
              stops: [
                  [0, "rgba(255, 255, 255, .5)"],
                  [1, "rgba(240, 240, 255, .7)"]
              ]
          },
          borderWidth: 2,
          plotBackgroundColor: "rgba(255, 255, 255, .4)",
          plotShadow: true,
          plotBorderWidth: 1
      )
      f.lang(thousandsSep: ",")
      f.colors(["#000000","#DF3F3F", "#90ed7d", "#f7a35c", "#8085e9", "#996633", "#f15c80", "#e4d354"])
    end

    @pages = Page.all
    @acc_town = City.all
    @acc = Accurancy.find_by_sql(["
      select c.city_name,c.id,p.page_name,a.temperature_a,a.wind_speed_a,a.humidity_a,a.pressure_a, a.day from accurancies a
      join cities c on c.id=a.city_id
      join pages p on p.id=a.page_id
      where c.id= '?'
      and p.id = '?'
      order by a.day
                            ", city_id, page_id])

  end

end
