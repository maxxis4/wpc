class CitiesController < ApplicationController
  def show()
    @towns = City.all
    city = City.get_id(params[:name])
    @weather = Array.new
    (0..5).each do |i|
      @weather[i] = Array.new
    end
    @weather_pred = WeatherPrediction.find_by_sql(["
          select * from weather_predictions_actual wpa
          where city_id = '?'
          order by day,hour
         ", city ])
    @weather_pred.each do |weat_pred|
      @weather[weat_pred.day].push(weat_pred)
    end
    @actual_weather = Weather.find_by_sql <<-SQL
      SELECT * FROM actual_weather_actual
      WHERE id = #{city};
    SQL

    @accur = Accurancy.find_by_sql([" select * from accurancies a join pages p on p.id = a.page_id where city_id = ? and page_id = 1 order by day",  city])

  end
end
