class WeatherPredictionsController < ApplicationController

  def WeatherPrediction_params
    params.require(:WeatherPrediction).permit(:date, :hour, :Temperature, :Wind_speed, :Humidity, :Weather )
  end
end
