class CountryController < ApplicationController
  def show
    @weather = Weather.find_by_sql <<-SQL
      SELECT * FROM actual_weather_actual
      WHERE id IN (
      SELECT main_city_id from countries
      )
    SQL
  end
end
