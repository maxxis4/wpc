class Accurancy < ActiveRecord::Base

  def self.set_acc
    connection = ActiveRecord::Base.connection
    city = City.find_by_sql("select id from cities")
    page= Page.find_by_sql("select id from pages")
    city.each do |cityid|
      page.each do |pageid|
        (0..5).each do |i|
          updated_values = Accurancy.find_by_sql <<-SQL
            SELECT COALESCE((AVG(temperature_a)).ROUND,0) as temp, COALESCE((AVG(wind_speed_a)).ROUND,0) as wind,
                   COALESCE((AVG(humidity_a)).ROUND,0) as hum, COALESCE((AVG(pressure_a)).ROUND,0) as pres
            FROM weather_predictions
            WHERE temperature_a is not null
            AND page_id= #{pageid.id}
            AND city_id= #{cityid.id}
            AND day =#{i}
            GROUP BY day,city_id,page_id
          SQL
          if updated_values[0].nil?
            connection.execute <<-SQL
            UPDATE accurancies
              SET  temperature_a = 0,
                   wind_speed_a = 0,
                   humidity_a = 0,
                   pressure_a = 0
              WHERE city_id=#{cityid.id}
              AND page_id=#{pageid.id}
              AND day = #{i}
            SQL
          else
            connection.execute <<-SQL
              UPDATE accurancies
                SET  temperature_a = #{updated_values[0].temp},
                     wind_speed_a = #{updated_values[0].wind},
                     humidity_a = #{updated_values[0].hum},
                     pressure_a = #{updated_values[0].pres}
                WHERE city_id =#{cityid.id}
                AND page_id=#{pageid.id}
                AND day = #{i}
            SQL
          end
        end
      end
    end
    connection.execute <<-SQL
      REFRESH MATERIALIZED VIEW weather_predictions_actual;
    SQL
    connection.close
  end

  def self.set_acc_to_wp
    connection = ActiveRecord::Base.connection
    connection.execute <<-SQL
      update weather_predictions AS wp
      set temperature_a = 100 - abs(wp.temperature - w.temperature)*10,
          wind_speed_a =  100 - abs(wp.wind_speed - w.wind_speed)*3,
          humidity_a =    100 - abs(wp.humidity - w.humidity),
          pressure_a =    100 - abs(wp.pressure - w.pressure)*5
      from weathers AS w
      where wp.weather_id=w.id;
    SQL
    connection.close
  end

end