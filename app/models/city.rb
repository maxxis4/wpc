class City < ActiveRecord::Base
  has_many :weathers
  has_many :weather_predictions
  belongs_to :country

  def self.get_id(name)
    city_hash = {"Bratislava" => 'Bratislava', "Trnava" => 'Trnava', "Nitra" => 'Nitra', "Žilina" => 'Zilina',
                  "Banská Bystrica" => 'banska-bystrica', "Prešov" => 'Presov', "Košice" => 'Kosice', "Detva" => 'detva',
                  "Gelnica" => 'gelnica', "Humenné" => 'humenne', "Kamenica nad Cirochou" => 'kamenica-nad-cirochou',
                  "Kežmarok" => 'kezmarok', "Lučenec" => 'lucenec', "Malacky" => 'malacky', "Martin" => 'martin', "Michalovce" => 'michalovce',
                  "Partizánske" => 'partizanske', "Piešťany" => 'piestany', "Poltár" => 'poltar', "Poprad" => 'poprad', "Prievidza" => 'prievidza',
                  "Revúca" => 'revuca', "Sliač" => 'sliac', "Stropkov" => 'stropkov', "Trebišov" => 'trebisov', "Vranov nad Topľou" => 'vranov-nad-toplou',
                  "Žarnovica" => 'zarnovica'}
    towns = City.all
    towns.each do |town|
      if city_hash[town.city_name] == name
        return town.id
        break
      end
    end
    return 1
  end

end
