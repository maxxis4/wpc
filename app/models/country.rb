class Country < ActiveRecord::Base
  has_many :cities
  def self.get_id(name)
    country_hash = { "bratislavsky-kraj" => 'Bratislavský kraj', "trnavsky-kraj" => 'Trnavský kraj', "nitriansky-kraj" => 'Nitriansky kraj',
                      "trenciansky-kraj" => 'Trenčiansky kraj', "zilinsky-kraj" => 'Žilinský kraj',
                      "banskobystricky-kraj" => 'Banskobystrický kraj', "presovsky-kraj" => 'Prešovský kraj', "kosicky-kraj" => 'Košický kraj' }
    countries = Country.all
    countries.each do |country|
      if country_hash[name] == country.name
        return country.id
        break
      end
    end
    return 1
  end
  def show

  end
end
