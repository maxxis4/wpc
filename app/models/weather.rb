class Weather < ActiveRecord::Base
  belongs_to :city
  has_many :weather_predictions
  has_many :pages, through: :weather_predictions

  require 'open-uri'
  require 'nokogiri'
  require 'patron'
  require 'httparty'
  require 'date'

  def self.download_actual_weather()
    city = City.find_by_sql("select * from cities")
    weather2=Array.new
    desc = Array.new
    html = HTTParty.get(URI.escape("http://www.meteo.sk/aktualne-pocasie/europa/slovenska-republika"))
    html.parsed_response
    doc = Nokogiri::HTML(html)
    params = [".stav_data_mesto", ".stav_data_teplota", ".stav_data_vietor_sp", ".stav_data_vlhkost", ".stav_data_tlak"]
    weather = doc.search('.stav_data')
    params.each_with_index do |par,i|
      count = 0
      weather2[i] = Array.new
      weather.search("#{par}").each do |poc|
        if poc.text == '-'
          weather2[i][count] = -1
        else
          weather2[i][count] = poc.text
        end
        count +=1
      end
    end
    count = 0
    weather.xpath('//img[@class="stav_data_iko_stav"]/@alt').each do |poc|
      desc[count] = poc.text
      count +=1
    end
    connection = ActiveRecord::Base.connection
    city.each do |town|
      pom = weather2[0].index(town.city_name)
      #puts weather2[4][pom]
      Weather.create("city_id" => town.id,
                     "date" => "#{(Time.now.utc+2.hours).to_date}",
                     "hour" => (Time.now.utc+2.hours).strftime("%H"),
                     "temperature" => weather2[1][pom],
                     "wind_speed" => weather2[2][pom],
                     "humidity" => weather2[3][pom],
                     "pressure" => weather2[4][pom],
                     "description" => desc[pom])
    end
    connection.execute <<-SQL
      REFRESH MATERIALIZED VIEW actual_weather_actual
    SQL
    connection.close
  end
end
