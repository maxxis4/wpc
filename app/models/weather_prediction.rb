class WeatherPrediction < ActiveRecord::Base
  belongs_to :page
  belongs_to :city
  belongs_to :weather
  require 'open-uri'
  require 'nokogiri'
  require 'patron'
  require 'date'

  def self.download_wp_smesk()
    city = City.find_by_sql("select id,city_name from cities")
    city_hash = {"Bratislava" => :Bratislava, "Trnava" => :Trnava, "Nitra" => :Nitra, "Žilina" => :Zilina,
                 "Banská Bystrica" => 'banska-bystrica', "Prešov" => :Presov, "Košice" => :Kosice, "Detva" => :detva,
                 "Gelnica" => :gelnica, "Humenné" => :humenne, "Kamenica nad Cirochou" => 'kamenica-nad-cirochou',
                 "Kežmarok" => :kezmarok, "Lučenec" => :lucenec, "Malacky" => :malacky, "Martin" => :martin, "Michalovce" => :michalovce,
                 "Partizánske" => :partizanske, "Piešťany" => :piestany, "Poltár" => :poltar, "Poprad" => :poprad, "Prievidza" => :prievidza,
                 "Revúca" => :revuca, "Sliač" => :sliac, "Stropkov" => :stropkov, "Trebišov" => :trebisov, "Vranov nad Topľou" => 'vranov-nad-toplou',
                 "Žarnovica" => :zarnovica}
    @weather = Array.new
    sess = Patron::Session.new
    sess.timeout = 10
    sess.base_url = "http://pocasie.sme.sk/krajina/slovensko/"
    city.each do |town|
      @weather.drop(@weather.length)
      count = 0
      html = sess.get URI.escape("#{city_hash["#{town.city_name}"]}")
      doc = Nokogiri::HTML(html.body)
      weather_page = doc.search('.pocasie-table-body-wrap')
      (1..7).each do |i|
        pocasie = weather_page.search("#pocasie-menu-button-#{i}-body")
        #pocasie.search('.pocasie-table-col2, .pocasie-table-col1').each do |poc|
        @weather[count] = Array.new
        @weather[count] = pocasie.text.squish.split(' ') # - udaje pocasia
        count +=1
        #end
      end
      weather_page.xpath('//div[@class="pocasie-table-header-cell"]/img/@alt').each do |poc|
        @weather[count] = poc.text
        count +=1
      end

      (0..5).each do |i| # cykly kvoli pridavaniu a usporiadaniu udajov
        count = 0
        (0..7).each do |j|
          if j>=3 && j<=7 # nerovnomerne udaje
            count +=2
          end
          WeatherPrediction.create( "city_id" => town.id, "page_id" => 1, "weather_id" => nil,
                                    "day" => i,
                                    "date" => DateTime.now.to_date+i+1,
                                    "hour" => @weather[i][18+7*j].to(-4),
                                    "temperature" => @weather[i][19+7*j].to(-3),
                                    "wind_speed" => @weather[i][90+6*j+count],
                                    "humidity" => @weather[i][92+6*j+ count].to(-2),
                                    "pressure" => @weather[i][93+6*j+ count].to_i,
                                    "description" => @weather[7+j+i*8])
        end
      end

    end

  end

  def self.download_wp_pocasiesk()
    city = City.find_by_sql("select id,city_name from cities")
    city_hash = {"Bratislava" => :Bratislava, "Trnava" => :Trnava, "Nitra" => :Nitra, "Žilina" => :Zilina,
                 "Banská Bystrica" => 'banska-bystrica', "Prešov" => :Presov, "Košice" => :Kosice, "Detva" => :detva,
                 "Gelnica" => :gelnica, "Humenné" => :humenne, "Kamenica nad Cirochou" => 'kamenica-nad-cirochou',
                 "Kežmarok" => :kezmarok, "Lučenec" => :lucenec, "Malacky" => :malacky, "Martin" => :martin, "Michalovce" => :michalovce,
                 "Partizánske" => :partizanske, "Piešťany" => :piestany, "Poltár" => :poltar, "Poprad" => :poprad, "Prievidza" => :prievidza,
                 "Revúca" => :revuca, "Sliač" => :sliac, "Stropkov" => :stropkov, "Trebišov" => :trebisov, "Vranov nad Topľou" => 'vranov-nad-toplou',
                 "Žarnovica" => :zarnovica}
    weather_pred_hours = [2,5,8,11,14,17,20,23]
    sess = Patron::Session.new
    sess.timeout = 10
    sess.base_url = "http://www.pocasie.sk/slovensko/"
    city.each do |town|
      html = sess.get URI.escape("#{city_hash["#{town.city_name}"]}/h.html")
      doc = Nokogiri::HTML(html.body)
      weather_page = doc.search('.daily-forecast')
      last = 0
      day = -1
      weather_page.each do |weather|
        hour = weather.search('.date').text.to_i
        if last != hour
          last = hour
          if hour == 2 then day +=1 end
          if weather_pred_hours.include? hour
            WeatherPrediction.create( "city_id" => town.id, "page_id" => 2, "weather_id" => nil,
                                      "day" => day,
                                      "date" => DateTime.now.to_date+day+1, "hour" => hour,
                                      "temperature" => weather.search('.day').text.to_i,
                                      "wind_speed" => weather.search('.additional-info').text.split(' ')[1],
                                      "humidity" => weather.search('.additional-info').text.squish.split(' ')[4].to_i,
                                      "pressure" => -1,
                                      "description" => weather.search('.info').text.squish.split(',')[0])
          end
        end
      end
    end

  end

  def self.download_wp_meteoboxsk()
    city = City.find_by_sql("select id,city_name from cities")
    city_hash = {"Bratislava" => :bratislava, "Trnava" => :trnava, "Nitra" => :nitra, "Žilina" => :zilina,
                 "Banská Bystrica" => 'banska-bystrica', "Prešov" => :presov, "Košice" => :kosice, "Detva" => :detva,
                 "Gelnica" => :gelnica, "Humenné" => :humenne, "Kamenica nad Cirochou" => 'kamenica-nad-cirochou',
                 "Kežmarok" => :kezmarok, "Lučenec" => :lucenec, "Malacky" => :malacky, "Martin" => :martin, "Michalovce" => :michalovce,
                 "Partizánske" => :partizanske, "Piešťany" => :piestany, "Poltár" => :poltar, "Poprad" => :poprad, "Prievidza" => :prievidza,
                 "Revúca" => :revuca, "Sliač" => :sliac, "Stropkov" => :stropkov, "Trebišov" => :trebisov, "Vranov nad Topľou" => 'vranov-nad-toplou',
                 "Žarnovica" => :zarnovica}
    sess = Patron::Session.new
    sess.timeout = 10
    sess.base_url = "https://meteobox.sk/"
    city.each do |town|
      html = sess.get URI.escape("#{city_hash["#{town.city_name}"]}/petdenna-predpoved/")
      doc = Nokogiri::HTML(html.body)
      (0..5).each do |i|
        weather_page = doc.search(".po#{(DateTime.now.to_date+i+1).strftime("%Y%m%d")}")
        weather_page.shift
        weather_page.each do |weather|
          w = weather.text.split(' ')
          if w[1] == "dážď"
            WeatherPrediction.create( "city_id" => town.id, "page_id" => 3, "weather_id" => nil,
                                      "day" => i,
                                      "date" => (DateTime.now.to_date+i+1).strftime("%Y%m%d"),
                                      "hour" => w[0].to_i,
                                      "temperature" => w[2].to_i,
                                      "wind_speed" => w[7].to_i,
                                      "humidity" => w[12].to_i,
                                      "pressure" => w[10].to_i,
                                      "description" => w[1])
          else
            if w[1] == "možnosť" || w[1] == "dažďové"
              WeatherPrediction.create( "city_id" => town.id, "page_id" => 3, "weather_id" => nil,
                                        "day" => i,
                                        "date" => (DateTime.now.to_date+i+1).strftime("%Y%m%d"),
                                        "hour" => w[0].to_i,
                                        "temperature" => w[3].to_i,
                                        "wind_speed" => w[8].to_i,
                                        "humidity" => w[13].to_i,
                                        "pressure" => w[11].to_i,
                                        "description" => "#{w[1]} #{w[2]}")
            else
              WeatherPrediction.create( "city_id" => town.id, "page_id" => 3, "weather_id" => nil,
                                        "day" => i,
                                        "date" => (DateTime.now.to_date+i+1).strftime("%Y%m%d"),
                                        "hour" => w[0].to_i,
                                        "temperature" => w[2].to_i,
                                        "wind_speed" => w[6].to_i,
                                        "humidity" => w[11].to_i,
                                        "pressure" => w[9].to_i,
                                        "description" => w[1])
            end
          end
        end
      end
    end

  end

  def self.download_wp_freemeteosk()
    city = City.find_by_sql("select id,city_name from cities")
    city_hash = {"Bratislava" => :Bratislava, "Trnava" => :Trnava, "Nitra" => :Nitra, "Žilina" => :Zilina,
                 "Banská Bystrica" => 'banska-bystrica', "Prešov" => :Presov, "Košice" => :Kosice, "Detva" => :detva,
                 "Gelnica" => :gelnica, "Humenné" => :humenne, "Kamenica nad Cirochou" => 'kamenica-nad-cirochou',
                 "Kežmarok" => :kezmarok, "Lučenec" => :lucenec, "Malacky" => :malacky, "Martin" => :martin, "Michalovce" => :michalovce,
                 "Partizánske" => :partizanske, "Piešťany" => :piestany, "Poltár" => :poltar, "Poprad" => :poprad, "Prievidza" => :prievidza,
                 "Revúca" => :revuca, "Sliač" => :sliac, "Stropkov" => :stropkov, "Trebišov" => :trebisov, "Vranov nad Topľou" => 'vranov-nad-toplou',
                 "Žarnovica" => :zarnovica}
    gid_hash = {"Bratislava" => 3060972, "Trnava" => 3057124, "Nitra" => 3058531, "Žilina" => 3056508, "Banská Bystrica" => 3061186, "Prešov" => 723819,
                "Košice" => 724443, "Detva" => 3060589, "Gelnica" => 724800, "Humenné" => 724627, "Kamenica nad Cirochou" => 724535,
                "Kežmarok" => 724503, "Lučenec" => 3058986, "Malacky" => 3058897, "Martin" => 3058780, "Michalovce" => 724144,
                "Partizánske" => 3058268, "Piešťany" => 3058202, "Poltár" => 3058083, "Poprad" => 723846,
                "Prievidza" => 3058000, "Revúca" => 723747, "Sliač" => 3057576, "Stropkov" => 723455, "Trebišov" => 723358, "Vranov nad Topľou" => 723195,
                "Žarnovica" => 3056589}
    desc = [nil,'Jasno','Malá oblačnosť','Polooblačno','Oblačno','Možnosť dažďa','Slabý dážď','Dážď','Silný dážď','Možnosť búrok','Dážď a možnosť búrok',
            'Dážď a možnosť silných búrok','Možnosť dažďa alebo dažďa so snehom','Slabý dážď alebo dážď so snehom','Dážď alebo dážď so snehom',
            'Silný dážď alebo dážď so snehom','Dážď alebo dážď so snehom a možnosť búrok','Dážď alebo dážď so snehom a možnosť silných búrok',
            'Možnosť dažďa so snehom alebo sneženia','Slabý dážď so snehom alebo sneženie','Dážď so snehom alebo sneženie','Silný dážď so snehom alebo sneženie',
            'Dážď so snehom alebo sneženie a možnosť búrok','Dážď so snehom alebo sneženie a možnosť silných búrok','Možnosť sneženia','Slabé sneženie',
            'Sneženie','Silné sneženie','Sneženie a možnosť búrok','Sneženie a možnosť silných búrok','Jasno','Prevažne jasno','Polooblačno','Oblačno',
            'Prevažne jasno s možnosťou dažďa','Polooblačno s možnosťou dažďa','Oblačno s možnosťou prehánok','Slabý dážď, miestami prechodne jasno',
            'Oblačno so slabými dažďovými prehánkami','Dážď, miestami prechodne jasno','Oblačno, miestami prechodne dážď',
            'Silné dažďové prehánky, miestami prechodne jasno','Oblačno, miestami prechodne silný dážď','Slabé dažďové prehánky','Dažďové prehánky',
            'Miestami prechodne jasno a dážď s možnosťou búrok','Miestami prechodne jasno a dážď s možnosťou silných búrok','Oblačno a daždivo s možnosťou búrok',
            'Oblačno a daždivo s možnosťou silných búrok','Prevažne jasno s možnosťou dažďa alebo dažďových prehánok so snehom',
            'Polooblačno s možnosťou dažďa alebo dažďových prehánok so snehom','Oblačno s možnosťou dažďa alebo dažďových prehánok so snehom',
            'Slabý dážď alebo dažďové prehánky so snehom, miestami prechodne jasno','Oblačno so slabým dažďom alebo dažďovými prehánkami so snehom',
            'Dážď alebo dažďové prehánky so snehom, miestami prechodne jasno','Oblačno s dažďom alebo dažďovými prehánkami so snehom',
            'Silný dážď alebo dažďové prehánky so snehom, miestami prechodne jasno','Oblačno, miestami prechodne silný dážď alebo dažďové prehánky so snehom',
            'Slabý dážď alebo dažďové prehánky so snehom','Dážď alebo dažďové prehánky so snehom',
            'Miestami prechodne jasno a dážď alebo dažďové prehánky so snehom s možnosťou búrok',
            'Miestami prechodne jasno a dážď alebo dažďové prehánky so snehom s možnosťou silných búrok',
            'Oblačno s dažďom alebo dažďovými prehánkami so snehom a možnosťou búrok',
            'Oblačno s dažďom alebo dažďovými prehánkami so snehom a možnosťou silných búrok','Prevažne jasno s možnosťou dažďa so snehom alebo snehových prehánok',
            'Polooblačno s možnosťou dažďa so snehom alebo snehových prehánok','Oblačno s možnosťou dažďa so snehom alebo snehových prehánok',
            'Slabý dážď so snehom alebo snehové prehánky, miestami prechodne jasno','Oblačno so slabým dažďom so snehom alebo snehovými prehánkami',
            'Dážď so snehom alebo snehové prehánky, miestami prechodne jasno','Oblačno s dažďom so snehom alebo snehovými prehánkami',
            'Silný dážď so snehom alebo snehové prehánky, miestami prechodne jasno','Oblačno, miestami prechodne silný dážď so snehom alebo snehové prehánky',
            'Slabý dážď so snehom alebo snehové prehánky','Dážď so snehom alebo snehové prehánky',
            'Miestami prechodne jasno a dážď so snehom alebo sneženie s možnosťou búrok',
            'Miestami prechodne jasno a dážď so snehom alebo sneženie s možnosťou silných búrok',
            'Oblačno s dažďom so snehom alebo snehovými prehánkami a možnosťou búrok',
            'Oblačno s dažďom so snehom alebo snehovými prehánkami a možnosťou silných búrok','Prevažne jasno s možnosťou snehových prehánok',
            'Polooblačno s možnosťou snehových prehánok','Oblačno s možnosťou snehových prehánok','Slabé snehové prehánky, miestami prechodne jasno',
            'Oblačno so slabými snehovými prehánkami','Snehové prehánky, miestami prechodne jasno','Oblačno so snehovými prehánkami',
            'Silné snehové prehánky, miestami prechodne jasno','Oblačno, miestami prechodne silné snehové prehánky','Slabé snehové prehánky','Snehové prehánky',
            'Miestami prechodne jasno a snehové prehánky s možnosťou búrok','Miestami prechodne jasno a snehové prehánky s možnosťou silných búrok',
            'Oblačno so snehovými prehánkami a možnosťou búrok', 'Oblačno so snehovými prehánkami a možnosťou silných búrok', 'Freezing fog']
    sess = Patron::Session.new
    sess.timeout = 10
    sess.base_url = "http://freemeteo.sk/pocasie/"
    days = [:dnes,:zajtra,:den2,:den3,:den4,:den5]
    city.each do |town|
      days.each_with_index do |day,i|
        html = sess.get URI.escape("#{city_hash["#{town.city_name}"]}/hodinova-predpoved/#{day}/?gid=#{gid_hash["#{town.city_name}"]}&language=slovak&country=slovakia")
        doc = Nokogiri::HTML(html.body)
        weather_page = doc.search('.today table')
        weather_page = weather_page.text.split(' ')
        temp_index = weather_page.index("Tepl.")
        wind_index = weather_page.index("Vietor")
        hum_index = weather_page.index("vlhkosť")
        pres_index = weather_page.index("Tlak")
        desc_index = Array.new
        (10..temp_index).each do |ind|
          num = weather_page[ind].scan(/\d+/).first.to_i
          if num == 0
            next
          else
            desc_index.push(num)
          end
        end
        (0..6).each_with_index do |a,j|
           WeatherPrediction.create( "city_id" => town.id, "page_id" => 4, "weather_id" => nil,
                                     "day" => i,
                                     "date" => (DateTime.now.to_date+i+1).strftime("%Y%m%d"),
                                     "hour" => weather_page[1+j].to_i,
                                     "temperature" => weather_page[temp_index+1+j].to_i,
                                     "wind_speed" => weather_page[wind_index+1+2*j].split('°')[1].to_i,
                                     "humidity" => weather_page[hum_index+1+j].to_i,
                                     "pressure" => weather_page[pres_index+1+j],
                                     "description" => desc[desc_index[j].to_i])
        end
        WeatherPrediction.create( "city_id" => town.id, "page_id" => 4, "weather_id" => nil,
                                  "day" => i+1,
                                  "date" => (DateTime.now.to_date+i+2).strftime("%Y%m%d"),
                                  "hour" => weather_page[1+7].to_i,
                                  "temperature" => weather_page[temp_index+1+7].to_i,
                                  "wind_speed" => weather_page[wind_index+1+2*7].split('°')[1].to_i,
                                  "humidity" => weather_page[hum_index+1+7].to_i,
                                  "pressure" => weather_page[pres_index+1+7],
                                  "description" => desc[desc_index[7].to_i])
      end
    end
  end

  def self.download_wp_svieticom()
    city = City.find_by_sql("select id,city_name from cities")
    city_hash = {"Bratislava" => 'bratislava-1', "Trnava" => 'trnava-5', "Nitra" => 'nitra-14', "Žilina" => 'zilina-6',
                 "Banská Bystrica" => 'banska-bystrica-15', "Prešov" => 'presov-13', "Košice" => 'kosice-2', "Detva" => 'detva-55',
                 "Gelnica" => 'gelnica-102', "Humenné" => 'humenne-23', "Kamenica nad Cirochou" => 'kamenica-nad-cirochou-1907',
                 "Kežmarok" => 'kezmarok-50', "Lučenec" => 'lucenec-28', "Malacky" => 'malacky-48', "Martin" => 'martin-10', "Michalovce" => 'michalovce-19',
                 "Partizánske" => 'partizanske-32', "Piešťany" => 'piestany-3', "Poltár" => 'poltar-109', "Poprad" => 'poprad-7', "Prievidza" => 'prievidza-4',
                 "Revúca" => 'revuca-59', "Sliač" => 'sliac-11', "Stropkov" => 'stropkov-69', "Trebišov" => 'trebisov-38', "Vranov nad Topľou" => 'vranov-nad-toplou-36',
                 "Žarnovica" => 'zarnovica-101'}
    sess = Patron::Session.new
    sess.timeout = 10
    sess.base_url = "http://www.svieti.com/mista/"
    city.each do |town|
      (0..5).each do |day|
        html = sess.get URI.escape("#{city_hash["#{town.city_name}"]}/den-#{day+1}")
        doc = Nokogiri::HTML(html.body)
        weather_page = doc.search('.detail-predpoved')
        weather_temp = weather_page.search('#teplota').text.split('°')
        weather_wind = weather_page.search('#vitr').text.split(' m/s')
        weather_pres = weather_page.search('#tlak').text.split(' hPa')
        weather_desc = Array.new
        weather_page.xpath('//span[@id="ikona"]/img/@alt').each do |poc|
          weather_desc.push(poc.text)
        end
        [2,5,8,11,14,17,20,23].each do |hour|
          WeatherPrediction.create( "city_id" => town.id, "page_id" => 5, "weather_id" => nil,
                                    "day" => day,
                                    "date" => (DateTime.now.to_date+day+1).strftime("%Y%m%d"),
                                    "hour" => hour,
                                    "temperature" => weather_temp[hour].to_i,
                                    "wind_speed" => (weather_wind[hour].to_i)*3.6,
                                    "humidity" => -1,
                                    "pressure" => weather_pres[hour].to_i,
                                    "description" => weather_desc[hour])
        end
      end
    end

  end

  def self.join_wp_on_aw
    weather = Weather.find_by_sql <<-SQL
      SELECT * FROM weathers
      WHERE date = current_date-1
    SQL
    connection = ActiveRecord::Base.connection
    weather.each do |weat|
      connection.execute <<-SQL
        UPDATE weather_predictions AS wp
          SET weather_id = #{weat.id}
          WHERE wp.date = current_date-1
          AND wp.hour = #{weat.hour}
          AND wp.city_id = #{weat.city_id}
      SQL
    end
    connection.close
  end

  def self.join_whole_table
    weather = Weather.find_by_sql <<-SQL
      SELECT * FROM weathers
    SQL
    connection = ActiveRecord::Base.connection.raw_connection
    connection.prepare("Update_table","UPDATE weather_predictions AS wp
          SET weather_id = $1
          WHERE wp.date = $2
          AND wp.hour = $3
          AND wp.city_id = $4")
    weather.each do |weat|
      connection.exec_prepared('Update_table', [ weat.id,weat.date,weat.hour,weat.city_id])
    end
  end
end
