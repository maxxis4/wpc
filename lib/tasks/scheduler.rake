namespace :scheduler do

  task update_weather: :environment do
    puts "Updating actual weather"
    Weather.download_actual_weather
    puts "Updating actual weather done"
  end

  task update_weather_prediction_smesk: :environment do
    puts "Updating weather prediction - sme.sk"
    WeatherPrediction.download_wp_smesk
    puts "Updating weather prediction - sme.sk - done "
  end

  task update_weather_prediction_pocasiesk: :environment do
    puts "Updating weather prediction - pocasie.sk"
    WeatherPrediction.download_wp_pocasiesk
    puts "Updating weather prediction - pocasie.sk - done "
  end

  task update_weather_prediction_meteoboxsk: :environment do
    puts "Updating weather prediction - meteobox.sk"
    WeatherPrediction.download_wp_meteoboxsk
    puts "Updating weather prediction - meteobox.sk - done"
  end

  task update_weather_prediction_freemeteosk: :environment do
    puts "Updating weather prediction - freemeteo.sk"
    WeatherPrediction.download_wp_freemeteosk
    puts "Updating weather prediction - freemeteo.sk - done"
  end

  task update_weather_prediction_svieticom: :environment do
    puts "Updating weather prediction - svieti.com"
    WeatherPrediction.download_wp_svieticom
    puts "Updating weather prediction - svieti.com - done"
  end

  task update_accurancy: :environment do
    puts "Updating accurancy"
    puts "Joining WP and AC"
    WeatherPrediction.join_wp_on_aw
    puts "Updating accurancy for lines in wp"
    Accurancy.set_acc_to_wp
    puts "Updating accurancy for pages and towns"
    Accurancy.set_acc
    puts "Updating accurancy done"
  end

  task update_whole_table: :environment do
    puts "Updating accurancy for whole table"
    WeatherPrediction.join_whole_table
    puts "Updating accurancy for whole table - done"
  end

end
